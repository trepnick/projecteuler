#!/usr/bin/env ruby
require 'prime'
#find the sum of primes below 2 million

sum = 0

Prime.each(2000000) {|a| sum += a}

puts sum
