#!/usr/bin/env ruby

# find the sum of all even fibonacci numbers below 4000000

def sum_evens(a,b,c)
  if b < 4000000
    if b%2 == 0
      c += b
    end
    c = sum_evens(b,a + b,c)
  end
  return c
end

puts sum_evens(1,1,0)
