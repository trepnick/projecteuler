#!/usr/bin/env ruby

#find the difference betweek the sum of the squares of the first 100 natural numbers and the square of the sum

sumsq = 0
sqsum = 0

for i in 1..100
	c = i
	c *= c
	sumsq += c
end

for i in 1..100
	c = i
	sqsum += c
end

sqsum = sqsum ** 2
diff = sqsum - sumsq
puts diff
